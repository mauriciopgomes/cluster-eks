module "eks" {
  source            = "terraform-aws-modules/eks/aws"
  cluster_name      = "${var.name}-${var.env}"
  cluster_version   = "1.18"
  subnets           = module.vpc.public_subnets
  vpc_id            = module.vpc.vpc_id
  write_kubeconfig  = false
  tags = {
    environment                                       = var.env
    terraform                                         = "true"
    "k8s.io/cluster-autoscaler/enabled"               = "true"
    "k8s.io/cluster-autoscaler/${var.name}-${var.env}" = "owned"
  }

  node_groups = {
    spot-managed-1 = {
      desired_capacity = var.desired_capacity
      max_capacity     = var.max_capacity
      min_capacity     = var.min_capacity
      instance_types = var.override_instance_types
      capacity_type  = "SPOT"
      k8s_labels = {
        Environment = var.env
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
      }
      additional_tags = {
        ExtraTag = var.name
      }
    }
  }
}

#   workers_group_defaults = {
#     root_volume_type = "gp2"
#   }

#   worker_groups = [
#     {
#       name                  = "spot-2"
#       spot_price            = "0.0416"
#       instance_type         = "t3.medium"
#       asg_min_size          = 2
#       asg_desired_capacity  = 2
#       asg_max_size          = 20
#       kubelet_extra_args    = "--node-labels=node.kubernetes.io/lifecycle=spot"
#       suspended_processes   = ["AZRebalance"]
#     }
#   ]

#   depends_on = [
#     module.vpc,
#   ]
# }

