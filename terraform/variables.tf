variable "name" {
    type = string
}
variable "env" {
    type = string
}
variable "region" {
    type = string
}
variable "cidr" {
    type = string
}
variable "private_subnets" {
    type = list
}
variable "public_subnets" {
    type = list
}

variable "override_instance_types" {
    type = list
}
variable "min_capacity" {
    type = string
}
variable "max_capacity" {
    type = string
}
variable "desired_capacity" {
    type = string
}