terraform {
    backend "s3" {
        bucket = "tf-mauriciogomes"
        region = "us-east-1"
        key    = "eks/tf.state"
    }
}
